const passport = require('passport')
var GoogleStrategy = require('passport-google-oauth20').Strategy;


/*

{
  "web": {
    "client_id": "647232832555-3n9ljkot5uljau6tekf75606s4fopi0l.apps.googleusercontent.com",
    "project_id": "node-testing-1603246681245",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_secret": "YJToIAq4E6OcNEkbm9qf2T4X",
    "javascript_origins": ["http://localhost:8000"]
  }
}




*/

    passport.use(new GoogleStrategy({
        clientID: "647232832555-3n9ljkot5uljau6tekf75606s4fopi0l.apps.googleusercontent.com",
        clientSecret: "YJToIAq4E6OcNEkbm9qf2T4X",
        userProfileURL: 'https://www.googleapis.com/oauth2/v3/userinfo',
         callbackURL: 'http://localhost:8000/oauth2callback'
      },
      function(accessToken, refreshToken, profile, cb) {
       console.log(accessToken, refreshToken)
      }
    ));

passport.authenticate('google', { scope: ['profile'] })